﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VNG : MonoBehaviour
{
    private static List<VNCommand> commands;
    [SerializeField]
    Type startScriptType;
    VNGStoryScript startScript;

    private static List<VNCommand> Commands
    {
        get
        {
            if (commands == null)
            {
                commands = new List<VNCommand>();
            }
            return commands;
        }

        set
        {
            commands = value;
        }
    }

    public void OnEnable()
    {
        startScriptType = typeof(Scene000);
        startScript = (VNGStoryScript)Activator.CreateInstance(startScriptType);
    }

    // Start is called before the first frame update
    void Start()
    {
        startScript.DoScript();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    static public void Message(EVNCharacter character, string message, float time = -1)
    {
        Commands.Add(new VNCommandMessage(character, message, time));
    }
}
