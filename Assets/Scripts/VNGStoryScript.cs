﻿using UnityEngine;

public abstract class VNGStoryScript : Object
{
    public abstract void DoScript();
    //{
    //    Debug.Log("Doing VNGStoryScript, you need to override me");
    //}

}