﻿internal class VNCommand
{

}

class VNCommandMessage : VNCommand
{
    string message;
    EVNCharacter character;
    float time;

    public VNCommandMessage(EVNCharacter character, string message, float time)
    {
        this.character = character;
        this.message = message;
        this.time = time;
    }
}